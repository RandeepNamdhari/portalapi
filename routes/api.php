<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
      'middleware' => ['auth:api','cors','token_life']
    ], function() {
        Route::get('user/logout', 'UserController@logout');

        Route::get('user/show', 'UserController@edit');


        Route::get('user/{id}', 'UserController@show');

        Route::put('user/update/{id}','UserController@update')->middleware('scopes:update-email');
       

    });


Route::post('user/register','UserController@store');

Route::get('users','UserController@index');

Route::post('user/login/via/email','UserController@loginViaEmail');

Route::get('user/verify/{verify_code}','UserController@verify');

