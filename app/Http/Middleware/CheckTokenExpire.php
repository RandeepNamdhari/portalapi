<?php

namespace App\Http\Middleware;

use Closure;

class CheckTokenExpire
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=$request->user()->AuthToken()->latest()->first();
        if ($user->expires_at < \Carbon\Carbon::now()) {

            $request->user()->token()->revoke(); 
            
       return 403;
}
        return $next($request);
    }
}
