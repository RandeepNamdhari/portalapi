<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Postmark\PostmarkClient;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Validator;
use \Carbon\Carbon;


class UserController extends Controller
{

    protected $expire_time;
    protected $from;


    function __construct()
    {
      $this->expire_time = env('LINK_EXPIRE_TIME');
      $this->from=env('EMAIL_FROM');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
           
           'email' => 'required|email|string|unique:users',
         
           ]);

        if ($validator->fails()) {

           return response()->json(['status'=>false,'message'=>'','error'=>$validator->errors()->first()], 401);      

       }

       $input = $request->only('email');

       try {

         $user = User::create($input);

         
        $code=Str::uuid().$user->id;

        $lifetime=Carbon::now()->addMinutes($this->expire_time);

        $user->verify_code()->create(['code'=>$code,'expires_at'=>$lifetime]);

         $emailBody='Please click the the verify button.<a style="background:lightgreen;padding:10px;" href='.env('PORTAL_URL').'user/verify/'.$code.'>Verify</a>';

         $to=$user->email;


         $this->sendMail($to,$emailBody);

         $message='Thank you,You will receive a login link via email soon.';

         return array('status'=>true,'message'=>$message,'error'=>'');

       } catch (Exception $e) {

         return response()->json(array('status'=>false,'message'=>'','error'=>$e->getMessage()),403);
         
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        try {

            $user= User::find($id);

            $status=true;

            $message='success';

        } catch (Exception $e) {
            
            $error=$e->getMessage();
            $status=false;
        }

        return array('status'=>$status,'data'=>$user,'message'=>$message??'','error'=>$error??'');
        


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
             $user=User::find($id);

             $user->email=$request->email;

             $user->save();

             $status=true;

            $message='Your email is updated successfully.';
            
        } catch (Exception $e) {

            $status=false;

            $error=$e->getMessage();
            
        }

        return array('status'=>$status,'data'=>$user,'message'=>$message??'','error'=>$error??'');
       



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendMail($to='',$message='',$subject='Login')
    {
        $client = new PostmarkClient(env('POSTMARK_CLIENT'));


     $sendResult = $client->sendEmail(
                  $this->from,
                  $to,
                  $subject,
                  $message);
    }

    public function loginViaEmail(Request $request)
    {
      

        $request->validate(['email'=>'required|email']);


        if($user=User::where('email',$request->email)->first()):
        
            if ($user != null)
            {
                 \Auth::loginUsingId($user->id);
            }

          
          
        $tokenResult = $user->createToken('Personal Access Token',['update-email']);
        

        $token = $tokenResult->token;
    
        $user->life=$token->expires_at = $user->verify_code()->latest()->first()->expires_at;
        
        $token->save();



        $user->token = $tokenResult->accessToken;

        
         

           return array('status'=>true,'message'=>'You are login successfully.','data'=>$user);

           else:
           
            return array('status'=>false,'message'=>'','error'=>'You are not registering with us.');
           

        endif;
    }

    public function verify(string $verify_code)
    {
       try {

          $verify_code=\App\VerifyCode::where('code',$verify_code)->latest()->first();



          if($verify_code && $verify_code->expires_at > Carbon::now()):

            $user=$verify_code->user??null;



        return array('status'=>true,'message'=>'Your are verified successfully.','data'=>$user);

      else:

        return array('status'=>false,'message'=>'','error'=>'Your Account is expired or not exist.');

      endif;


       } catch (Exception $e) {
         
         return array('status'=>false,'message'=>'','error'=>'Server Error please try later.');

       }
    }


}
