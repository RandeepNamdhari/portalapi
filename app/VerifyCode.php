<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyCode extends Model
{
    protected $fillable=['code','expires_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
