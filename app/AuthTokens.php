<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthTokens extends Model
{
    protected $table='oauth_access_tokens';
}
